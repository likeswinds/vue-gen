#!/usr/bin/env node
var inquirer = require('inquirer')
var option = require('./bin/option.js')
var colors = require('colors')
var gen = require('./bin/gen')

console.log(`读取默认配置......`.yellow)
for (let k in option) {
  const opt = option[k]
  console.log(`${opt.desc}`.cyan, `${opt.value}`.green)
}

gen('access', option)
