#!/usr/bin/env node
var request = require('axios')
var ejs = require('ejs')
var fs = require('fs')
var colors = require('colors')
var mkFile = require('./mkFile')
var inquirer = require('inquirer')

/**
 * 代码生成
 * @param {String} table 要生成的表
 * @param {*} option 代码生成配置
 */
async function gen(model, option) {
  const { apiDocUrl } = option
  const url = apiDocUrl.value + model + '/api-docs'
  console.log(`获取api文档url`.cyan, `${url}`.green)
  const res = await request({
    url: url,
    method: 'get'
  })
  const { definitions, paths, basePath } = res.data
  console.log(`获取资源成功`.cyan)

  const keys = unique(Object.keys(definitions))
  let api = {}
  // 选择要生成的文档
  const answ = await inquirer.prompt({
    type: 'list',
    name: 'doc',
    message: '请选择要生成的对象\n'.yellow,
    pageSize: 20,
    choices: keys.filter(item => !item.startsWith('Page'))
  })
  console.log(answ)
  api = definitions[answ.doc]
  const table = answ.doc
    .replace('Vo', '')
    .replace('Dto', '')
    .toLowerCase()
  const props = []
  // 重新构建 model
  for (const prop in api.properties) {
    const col = api.properties[prop]
    if (col.$ref || (col.items && col.items.$ref)) {
      let $ref = col.$ref
      if (col.type === 'array') {
        $ref = col.items.$ref
      }
      const ref = $ref.substr($ref.lastIndexOf('/') + 1, $ref.length)
      const obj = definitions[ref]
      for (const o in obj.properties) {
        const field = prop + '.' + o
        if (!obj.properties[o].description) {
          obj.properties[o].description =
            api.properties[prop].description + '.' + o
        }
        api.properties[field] = obj.properties[o]
      }
      delete api.properties[prop]
    }
  }
  // 重新构建url对象
  const data = []
  for (const url in paths) {
    const opt = paths[url]
    for (const method in opt) {
      const api = opt[method]
      if (api.tags[0].includes(table)) {
        const urlPath = []
        api.parameters
          .filter(item => item.in === 'path')
          .forEach((v, i) => urlPath.push('_' + v.name))
        const urlParam = []
        api.parameters
          .filter(item => item.in === 'query')
          .forEach((v, i) => {
            const name = v.name
            if (name.indexOf('.') > 0) {
              name = name.substr(0, name.indexOf('.'))
            }
            if (!urlParam.includes(name)) {
              urlParam.push(v.name)
            }
          })
        const urlBody = []
        api.parameters
          .filter(item => item.in === 'body')
          .forEach((v, i) => urlBody.push(v.name))
        let func = api.operationId.substr(0, api.operationId.indexOf('Using'))
        const funcParams = []
        if (urlPath.length > 0) {
          funcParams.push(...urlPath)
        }
        if (urlParam.length > 0) {
          if (urlParam.length === 1) {
            funcParams.push(urlParam[0])
          } else {
            funcParams.push('_params')
          }
        }
        if (urlBody.length > 0) {
          funcParams.push('_data')
        }
        if (func === 'delete') {
          func = 'del'
        }
        const dt = {
          url,
          basePath,
          func,
          funcParams,
          method,
          urlPath,
          urlParam,
          urlBody,
          desc: api.description,
          paramsDesc: api.parameters
        }
        data.push(dt)
      }
    }
  }
  api.model = model
  api.table = table.toLowerCase()
  const files = []
  // api.js
  ejs.renderFile(
    'bin/template/api.ejs',
    { apis: data, basePath },
    (err, text) => {
      files.push({
        path: `${option.apiPath.value}${model}`,
        file: `${table}.js`,
        text,
        name: 'api',
        type: '.js'
      })
    }
  )
  // index.vue
  ejs.renderFile('bin/template/index.ejs', api, (err, text) => {
    files.push({
      path: option.vuePath.value + table,
      file: 'index.vue',
      text,
      name: 'index',
      type: '.vue'
    })
  })
  // add.vue
  ejs.renderFile(
    'bin/template/add.ejs',
    { apis: data, basePath },
    (err, text) => {
      files.push({
        path: `${option.vuePath.value}${table}\\template`,
        file: `add.vue`,
        text,
        name: 'add',
        type: '.vue'
      })
    }
  )
  // info.vue
  ejs.renderFile(
    'bin/template/info.ejs',
    { apis: data, basePath },
    (err, text) => {
      files.push({
        path: `${option.vuePath.value}${table}\\template`,
        file: `info.vue`,
        text,
        name: 'info',
        type: '.vue'
      })
    }
  )
  // modify.vue
  ejs.renderFile(
    'bin/template/modify.ejs',
    { apis: data, basePath },
    (err, text) => {
      files.push({
        path: `${option.vuePath.value}${table}\\template`,
        file: `modify.vue`,
        text,
        name: 'modify',
        type: '.vue'
      })
    }
  )
  const choices = []
  const cfile = []
  // 判断文件是否已存在，若存在则询问是否覆盖
  for (const file of files) {
    // 获取命令行所在路径
    let fPath = process.cwd() + '\\' + file.path
    // 生成路径
    if (!fs.existsSync(fPath)) {
      mkFile(fPath)
      console.log(`生成路径：${fPath}`.yellow)
    }
    // 要生成的文件完整路径
    fPath += '\\' + file.file
    const ex = fs.existsSync(fPath)
    // 若路径已存在，构建询问对象
    if (ex) {
      choices.push('[' + file.name + file.type + ']')
      // 记录索引
      cfile[file.name] = { path: fPath, text: file.text }
    } else {
      fs.writeFileSync(fPath, data)
      console.log(`代码生成成功^_^`.red, `${fPath}`.green)
    }
  }
  if (choices.length > 0) {
    const question = {
      type: 'checkbox',
      name: 'cover',
      message: `以下文件已存在，请勾选要覆盖的文件`.yellow,
      choices
    }
    inquirer.prompt(question).then(answers => {
      if (answers.cover.length > 0) {
        for (const ans of answers.cover) {
          let name = ans.substr(1, ans.indexOf('.') - 1)
          fs.writeFileSync(cfile[name].path, cfile[name].text)
          console.log(
            '代码生成成功^_^'.red,
            `${cfile[name].path}`.green,
            '已覆盖'.blue
          )
        }
      }
    })
  }
}
function unique(arr) {
  const res = new Map()
  return arr.filter(a => !res.has(a) && res.set(a, 1))
}
module.exports = gen
