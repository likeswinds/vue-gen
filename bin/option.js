const option = {
  template: { desc: "模版路径", value: "template" },
  api: { desc: "是否生成api", value: true },
  apiPath: { desc: "api代码生成路径", value: "src\\api\\" },
  apiDocUrl: { desc: "api文档url", value: "http://172.20.5.41:10001/v3/" },
  vuePath: { desc: "vue代码生成路径", value: "src\\view\\" }
}
module.exports = option
