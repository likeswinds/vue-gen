var fs = require("fs")
var path = require("path")
function mkdirsSync(dirpath, mode) {
  try {
    if (!fs.existsSync(dirpath)) {
      let pathtmp
      dirpath.split(/[/\\]/).forEach(function(dirname) {
        //这里指用/ 或\ 都可以分隔目录  如  linux的/usr/local/services   和windows的 d:\temp\aaaa
        if (pathtmp) {
          pathtmp = path.join(pathtmp, dirname)
        } else {
          pathtmp = dirname
        }
        if (!fs.existsSync(pathtmp)) {
          if (!fs.mkdirSync(pathtmp, mode)) {
            return false
          }
        }
      })
    }
    return true
  } catch (e) {
    log.error("create director fail! path=" + dirpath + " errorMsg:" + e)
    return false
  }
}

module.exports = mkdirsSync
