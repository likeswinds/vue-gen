# 代码生成器

## get start

### 安装

```cmd
> npm install vgen-swagger

```

### 初始化

```cmd
> vgen init

```

### 代码生成

```cmd
> vgen
```

出现如下提示

```
读取默认配置......
模版路径 template
是否生成 api true
api 代码生成路径 src\api\
api 文档 url http://172.20.5.41:10001/v3/
vue 代码生成路径 src\view\
获取 api 文档 url http://172.20.5.41:10001/v3/access/api-docs
获取资源成功
? 请选择要生成的对象
(Use arrow keys)

> AppInfo
> DepartmentVo
> Resource
> RoleVo
> Sort
> UserBO
> UserDevice
> UserInfoVo
> UserLoginVo
> UserTokenVo
```
